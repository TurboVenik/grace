import asyncio
import random

from app.db import init_db, print_db, close, with_db
from app.db import Task

ROWS = 10


async def gen_data(rows: int):
    data = [
        Task(a=random.randint(1, 10), b=random.randint(1, 10))
        for _ in range(rows)]

    await Task.bulk_create(data)
    await print_db()


@with_db
async def main():
    print("GEN DATA")
    await gen_data(ROWS)
    print("GEN DATA DONE")


if __name__ == '__main__':
    asyncio.run(main())
