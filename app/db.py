from tortoise import Tortoise
from tortoise import fields
from tortoise.models import Model


class Task(Model):
    id = fields.IntField(pk=True)
    status = fields.IntField(default=0)

    a = fields.IntField()
    b = fields.IntField()

    res = fields.IntField(null=True, default=None)

    def __str__(self):
        return f"id={self.id} status={self.status} ({self.a} + {self.b} = {self.res})"

    def __repr__(self):
        return str(self)


async def print_db():
    for item in await Task.all():
        print(item)


async def init_db():
    await Tortoise.init(
        db_url='sqlite://tmp/db.sqlite3',
        modules={'models': ['app.db']}
    )
    await Tortoise.generate_schemas()


async def close():
    await Tortoise.close_connections()


def with_db(f):
    async def wrapper():
        await init_db()
        res = await f()
        await close()
        return res

    return wrapper
