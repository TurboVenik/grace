import asyncio
import random
from typing import List

from tortoise.expressions import Q

from app.db import init_db, with_db, Task


async def job(task: Task):
    task.status = 3
    await task.save()
    await asyncio.sleep(random.randint(1, 5))
    is_error = random.randint(1, 3) == 1
    if is_error:
        task.status = 99
        await task.save()
        print(f"Task {task.id} ERROR")
        return

    task.status = 1
    task.res = task.a + task.b
    await task.save()
    print(f"Task {task.id} {task.a}+{task.b}={task.res} DONE")


def print_queue(queue: List[Task]):
    for item in queue:
        print(item)


@with_db
async def main():
    print("START")
    queue = list(
        await Task.filter(~Q(status=1)).all()
    )
    print_queue(queue)
    index = 0
    tasks = []
    while index < len(queue):
        tasks.append(
            asyncio.create_task(job(queue[index]))
        )
        index += 1
        await asyncio.sleep(1)
    print("JOBS STARTED")

    await asyncio.gather(*tasks)
    print("JOBS DONE")

    print_queue(queue)


if __name__ == '__main__':
    asyncio.run(main())
