import asyncio

from app.db import print_db, with_db


@with_db
async def main():
    await print_db()


if __name__ == '__main__':
    asyncio.run(main())
