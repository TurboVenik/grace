import asyncio
import random
import signal
from typing import List

from tortoise.expressions import Q

from app.db import init_db, with_db, Task

INTERRUPTED = False
TASKS: List[asyncio.Task] = []


async def shutdown(sig_name):
    print(f"SIGNAL: {sig_name}")
    global INTERRUPTED
    INTERRUPTED = True
    for task in TASKS:
        task.cancel()


async def save_result(task: Task):
    is_error = random.randint(1, 3) == 1
    if is_error:
        task.status = 2
        await task.save()
        print(f"Task {task.id} ERROR")
        return

    task.status = 1
    task.res = task.a + task.b
    await task.save()
    print(f"Task {task.id} {task.a}+{task.b}={task.res} DONE")


async def job(task: Task):
    task.status = 3
    await task.save()
    await asyncio.sleep(random.randint(1, 5))
    try:
        await asyncio.shield(save_result(task))
    except asyncio.CancelledError:
        print("Cancel Shielded")


async def do_job(task: Task):
    try:
        await job(task)
    except asyncio.CancelledError:
        print(f"Task id={task.id} Canceled")
        task.status = 10
        await task.save()


def print_queue(queue: List[Task]):
    for item in queue:
        print(item)


@with_db
async def main():
    print("START")
    main_loop = asyncio.get_running_loop()
    try:
        main_loop.add_signal_handler(signal.SIGINT, lambda: asyncio.ensure_future(shutdown(signal.SIGINT)))
        main_loop.add_signal_handler(signal.SIGTERM, lambda: asyncio.ensure_future(shutdown(signal.SIGTERM)))
    except NotImplementedError:
        print("windows LOL")
    queue = list(
        await Task.filter(~Q(status=1)).all()
    )
    print_queue(queue)
    index = 0

    while index < len(queue) and not INTERRUPTED:
        TASKS.append(
            asyncio.create_task(do_job(queue[index]))
        )
        index += 1
        await asyncio.sleep(1)
    print("JOBS STARTED")

    await asyncio.gather(*TASKS)
    print("JOBS DONE")

    print_queue(queue)


if __name__ == '__main__':
    asyncio.run(main())
